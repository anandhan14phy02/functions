const functions = require('firebase-functions');
const admin=require('firebase-admin');
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
admin.initializeApp();


exports.notificationTrigger=functions.firestore.document("notifications/{Id}").onCreate((change,context)=>{
    return  admin.firestore().collection("notifications").doc("Tokens").get().then((snap)=>{
        console.log(snap)
        let message={
            data:{
                title:"Order Notification",
                body:change.data(),
            },
            token:snap.data()[change.data()['location']]
        }

        return admin.messaging().send(message).then((success)=>{
            console.log("Success");
            return Promise.resolve(0);
        }).catch((error)=>{
            console.log("Error");
            return Promise.resolve(0);
        });
    }).catch((error)=>{
        console.log("Error",error);
        return Promise.resolve(0);
    });

    

});
