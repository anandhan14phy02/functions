exports.createPDF=function(orderData,callback){
console.log("CartDetails in pdfCreater: \n"+ JSON.stringify(orderData['cartList']));
var buffer=[];
var dd = {
	content: [
		{   alignment:'center',
		    text:"Chettinad Groceries\n",
		    bold:true,
		    fontSize:20,
		    color:'red'
		},{
		    alignment:'center',
		    text:"xyz street, zz District\n\n",
		    fontSize:18,
		    color:'black'
		},
		{
			alignment: 'justify',
			color:'blue',
			columns: [
				{
					text: 'Billed to'
				},
				{
				    alignment:'center',
					text: 'Date of Issue'
				},
				{
				    alignment:'right',
				    text: 'Total'
				}
			]
		},"\n",
		{
			columns: [
				{
					text: orderData['name']+"\n"+orderData['address']
				},
				{
				    alignment:"center",
					text: '29/8/19'
				},
				{
				    alignment:'right',
					text:"Rs."+orderData['totalAmount']
				}
			]
		},{
		    color:"violet",
		    fontSize:20,
		    text:"\nCart Details:\n\n"},
		{
		    alignment:"center",
		    color:"blue",
		    columns:[
		        {
		            text:"ItemName"
		        },
		        {
		            text:"Unit Price"   
		        },
		        {
		            text:"Quantity"
		        },
		        {
		            text:"Price"
		        }]
		},"\n",
	],
	styles: {
		header: {
			fontSize: 18,
			bold: true
		},
		bigger: {
			fontSize: 15,
			italics: true
		},	
		subheader: {
			fontSize: 16,
			bold: true,
			margin: [0, 10, 0, 5]
		},
		tableExample: {
			margin: [0, 5, 0, 15]
		},
		tableHeader: {
			bold: true,
			fontSize: 13,
			color: 'black'
		}
	},
	defaultStyle: {
		columnGap: 20
	}
	
}
const carts=orderData['cartList'];
console.log('Carts:\n'+JSON.stringify(carts));
carts.forEach((cart)=>{
		  var subcost=cart['discountPrice']*cart['quantity'];
			dd['content'].push(
			 {
		        alignment:"center",
		        color:"black",
		        columns:[
		        { 
		            text: cart['name']
		           		        },
		        {
		           margin:[0,0,50,0], 
		           alignment:"right",
		           text:cart['discountPrice'] 
		           		        },
		        {   
		            margin:[0,0,50,0],
		            alignment:"right",
		           text:cart['quantity']
		            	        },
		        {
			    margin:[0,0,50,0],
			    alignment:"right",
		            text:""+subcost
		            }
		            ]
		         },"\n"
		         );
		});

const fonts={
	Roboto:{
    bold:"./fonts/roboto/Roboto-Medium.ttf",
	italics:"./fonts/roboto/Roboto-Italic.ttf",
	bolditalics:"./fonts/roboto/Roboto-MediumItalic.ttf",
	normal:"./fonts/roboto/Roboto-Regular.ttf"}
	};

const pdfmaker=require('pdfmake');
const printer=new pdfmaker(fonts);
const pdfDoc=printer.createPdfKitDocument(dd);
//pdfDoc.pipe(require('fs').createWriteStream('orderDetails.pdf'));
pdfDoc.end();
pdfDoc.on('data',buffer.push.bind(buffer));
pdfDoc.on('end',()=>{
			console.log("The end");
   	        callback(buffer);
                     });
}
