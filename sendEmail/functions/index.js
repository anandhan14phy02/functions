const functions = require('firebase-functions');
const nodemailer=require('nodemailer');
const admin=require('firebase-admin');
admin.initializeApp(functions.config.firestore);
const db=admin.firestore();

var createPDF=function (orderData,callback){

	return new Promise((resolve,reject)=>{
    console.log("CartDetails in pdfCreater: \n"+ JSON.stringify(orderData['cartList']));
	var buffer=[];
	var dd = {
	content: [
		{   alignment:'center',
		    text:"Chettinad Groceries\n",
		    bold:true,
		    fontSize:20,
		    color:'red'
		},{
		    alignment:'center',
		    text:"xyz street, zz District\n\n",
		    fontSize:18,
		    color:'black'
		},
		{
			alignment: 'justify',
			color:'blue',
			columns: [
				{
					text: 'Billed to'
				},
				{
				    alignment:'center',
					text: 'Date of Issue'
				},
				{
				    alignment:'right',
				    text: 'Total'
				}
			]
		},"\n",
		{
			columns: [
				{
					text: orderData['name']+"\n"+orderData['address']
				},
				{
				    alignment:"center",
					text: '29/8/19'
				},
				{
				    alignment:'right',
					text:"Rs."+orderData['totalAmount']
				}
			]
		},{
		    color:"violet",
		    fontSize:20,
		    text:"\nCart Details:\n\n"},
		{
		    alignment:"center",
		    color:"blue",
		    columns:[
		        {
		            text:"ItemName"
		        },
		        {
		            text:"Unit Price"   
		        },
		        {
		            text:"Quantity"
		        },
		        {
		            text:"Price"
		        }]
		},"\n",
	],
	styles: {
		header: {
			fontSize: 18,
			bold: true
		},
		bigger: {
			fontSize: 15,
			italics: true
		},	
		subheader: {
			fontSize: 16,
			bold: true,
			margin: [0, 10, 0, 5]
		},
		tableExample: {
			margin: [0, 5, 0, 15]
		},
		tableHeader: {
			bold: true,
			fontSize: 13,
			color: 'black'
		}
	},
	defaultStyle: {
		columnGap: 20
	}
	
}
const carts=orderData['cartList'];
console.log('Carts:\n'+JSON.stringify(carts));
carts.forEach((cart)=>{
		  var subcost=cart['discountPrice']*cart['quantity'];
			dd['content'].push(
			 {
		        alignment:"center",
		        color:"black",
		        columns:[
		        { 
		            text: cart['name']
		           		        },
		        {
		           margin:[0,0,50,0], 
		           alignment:"right",
		           text:cart['discountPrice'] 
		           		        },
		        {   
		            margin:[0,0,50,0],
		            alignment:"right",
		           text:cart['quantity']
		            	        },
		        {
			    margin:[0,0,50,0],
			    alignment:"right",
		            text:""+subcost
		            }
		            ]
		         },"\n"
		         );
		});

const fonts={
	Roboto:{
    bold:"./fonts/roboto/Roboto-Medium.ttf",
	italics:"./fonts/roboto/Roboto-Italic.ttf",
	bolditalics:"./fonts/roboto/Roboto-MediumItalic.ttf",
	normal:"./fonts/roboto/Roboto-Regular.ttf"}
	};

const pdfmaker=require('pdfmake');
const printer=new pdfmaker(fonts);
const pdfDoc=printer.createPdfKitDocument(dd);
//pdfDoc.pipe(require('fs').createWriteStream('orderDetails.pdf'));
pdfDoc.end();
pdfDoc.on('data',buffer.push.bind(buffer));
pdfDoc.on('end',()=>{
			console.log("The end");
   	        resolve(buffer);
                     });		
	});

}

var getUserInfo=function(userId,callback){
return new Promise((resolve,reject)=>{
	db.collection('user').doc(userId).get().then(document=>{		
		const ddata=document.data();
			   return resolve(ddata['email']);
        	}).catch((error)=>{

						});

		});
      }

var sendEmail=function(orderData,email,pdfDoc){
	return new Promise((resolve,reject)=>{
  	 console.log("Email from sendEmail: "+ email);
	 const transporter=nodemailer.createTransport({
            		service:'gmail',
            		secure:true,
            		host:'smtp.gmail.com',
            		auth:{
                		user:'anandhan1801@gmail.com',
                		pass:'A@18011997'
                		}     
        		});


     var messageOptions={
            		from:"GroAdmin",
		            to:email,
		            subject:"Grocer Order Details",
	                //text:"your order is "+orderData['status'],
            	 	//attachments:[{filename:"orderDetails.pdf",content:pdfDoc}]
        		}

     if(orderData['status']==='processing'){
     	 messageOptions['text']="Your Order is Confirmed";
     	 messageOptions['attachments']=[];
     	 messageOptions['attachments'].push({filename:"orderDetails.pdf",content:pdfDoc});
     	//messageOptions.push({text:"Your Order is Confirmed",attachments:[{filename:"orderDetails.pdf",content:pdfDoc}]});
     }
     else if(orderData['status']==='shipping') 
     {
     	messageOptions['text']="Your Order is on the way to your home.\nYour OrderId:"+orderData['id'];
     	messageOptions['attachments']=[];
     	//messageOptions.push({text:"Your Order is on the way to your home.\nYour OrderId:"+orderData['id']});
     }
     else if(orderData['status']==='canceled'){
     	messageOptions['text']="Sorry Unable to Process your Order!";
     	messageOptions['attachments']=[];
     }

     transporter.sendMail(messageOptions,(err,info)=>{
			if(err){
					reject(err);
				}
			else{
					resolve();
				}		
		});
	});
	   
}




exports.firebaseTrigger=functions.firestore.document('order/{docId}').onUpdate((change,context)=>
{
	   	    	const orderData=change.after.data();
        		const userId=orderData['userId'];		
       		 	return getUserInfo(userId).then((email)=>{
       			 		if(orderData['status']==="processing"){
       			 			return createPDF(orderData).then((bufferContent)=>{
	        								console.log("pdf created");
											const pdfDoc=Buffer.concat(bufferContent);
											return sendEmail(orderData,email,pdfDoc).then(()=>{
						 								return Promise.resolve(0);    		
																							  });
																				});
       			 		}
       			 		else{
       			 			return sendEmail(orderData,email,null).then(()=>{
						 								return Promise.resolve(0);    		
																			  });
       			 		}
       			 	});		
	    });
